package org.bsa.task.exception;

public class GifNotFoundException extends RuntimeException {
    private static final String DEFAULT_MSG = "Gif not found";

    public GifNotFoundException() {
        super(DEFAULT_MSG);
    }
}
