package org.bsa.task.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public final class Handler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(GifNotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(GifNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(IllegalUserIdException.class)
    public ResponseEntity<Object> handleBadRequestException(IllegalUserIdException ex) {
        return ResponseEntity.badRequest().build();
    }
}
