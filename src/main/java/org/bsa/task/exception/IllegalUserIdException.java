package org.bsa.task.exception;

public class IllegalUserIdException extends RuntimeException{
    private static final String DEFAULT_MSG = "Illegal user id";

    public IllegalUserIdException() {
        super(DEFAULT_MSG);
    }
}
