package org.bsa.task.controller;

import lombok.RequiredArgsConstructor;
import org.bsa.task.service.GifControllerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GifController {
    private final GifControllerService gifService;

    @GetMapping("/gifs")
    public List<String> getAllGifs() {
        return gifService.findAll();
    }
}
