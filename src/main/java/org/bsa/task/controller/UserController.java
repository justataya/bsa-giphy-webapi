package org.bsa.task.controller;

import lombok.RequiredArgsConstructor;
import org.bsa.task.dto.GifsByQueryDto;
import org.bsa.task.dto.HistoryEntryDto;
import org.bsa.task.dto.QueryDto;
import org.bsa.task.exception.GifNotFoundException;
import org.bsa.task.service.CacheControllerService;
import org.bsa.task.service.GifControllerService;
import org.bsa.task.service.UserControllerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserControllerService userService;
    private final GifControllerService gifControllerService;
    private final CacheControllerService cacheControllerService;

    @Value("${file.cache-dir}")
    private String cacheDir;

    @Value("${file.users-dir}")
    private String usersDir;

    @GetMapping("/{id}/all")
    public List<GifsByQueryDto> getAll(@PathVariable String id) {
        userService.validate(id);
        return userService.findAll(id);
    }

    @GetMapping("{id}/search")
    public String getGifByUser(@PathVariable String id, @RequestParam String query, @RequestParam(required = false) Boolean force) {
        userService.validate(id);
        if (force != null && force) {
            return ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/uploads/")
                    .path(findGifOnDisk(id, query))
                    .toUriString();
        }
        return findGifInCache(id, query);
    }

    private String findGifInCache(String id, String query) {
        String result = userService.findGifInCache(id, query);
        if (result == null) {
            return findGifOnDisk(id, query);
        }
        return result;
    }

    private String findGifOnDisk(String id, String query) {
        String result = userService.findGifOnDisk(id, query);
        if (result == null) {
            throw new GifNotFoundException();
        }
        return result;
    }

    @GetMapping("/{id}/history")
    public List<HistoryEntryDto> getHistory(@PathVariable String id) {
        userService.validate(id);
        return userService.findHistoryByUserId(id);
    }

    @DeleteMapping("/{id}/history/clean")
    public void deleteHistory(@PathVariable String id) {
        userService.validate(id);
        userService.deleteHistory(id);
    }

    @PostMapping("/{id}/generate")
    public String generateGif(@PathVariable String id, @RequestBody QueryDto queryDto) {
        userService.validate(id);
        if (queryDto.getForce() != null && queryDto.getForce()) {
            return ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/uploads/")
                    .path(generateRandomGif(id, queryDto))
                    .toUriString();
        }
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/uploads/")
                .path(findRandomGif(id, queryDto))
                .toUriString();
    }

    @DeleteMapping("/{id}/clean")
    public void deleteAll(@PathVariable String id) {
        userService.validate(id);
        userService.deleteAll(id);
    }

    private String generateRandomGif(String id, QueryDto queryDto) {
        String result = gifControllerService.findRandomGif(queryDto.getQuery(), usersDir + "/" + id);
        gifControllerService.copy(queryDto.getQuery(), cacheDir, result);
        userService.saveHistory(id, queryDto.getQuery(), result);
        return result;
    }

    private String findRandomGif(String id, QueryDto queryDto) {
        String result = gifControllerService.copy(queryDto.getQuery(), usersDir + "/" + id, cacheControllerService.findRandomByQuery(queryDto.getQuery()));
        if (result == null) {
            return generateRandomGif(id, queryDto);
        }
        userService.saveHistory(id, queryDto.getQuery(), result);
        return result;
    }

    @DeleteMapping("/{id}/reset")
    public void deleteCache(@PathVariable String id, @RequestParam(required = false) String query) {
        userService.validate(id);
        if (query == null) {
            userService.deleteUserCache(id);
        } else {
            userService.deleteUserCacheByQuery(id, query);
        }
    }
}
