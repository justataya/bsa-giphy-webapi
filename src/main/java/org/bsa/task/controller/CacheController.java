package org.bsa.task.controller;

import lombok.RequiredArgsConstructor;
import org.bsa.task.dto.GifsByQueryDto;
import org.bsa.task.dto.QueryDto;
import org.bsa.task.service.CacheControllerService;
import org.bsa.task.service.GifControllerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cache")
@RequiredArgsConstructor
public class CacheController {
    private final CacheControllerService cacheService;
    private final GifControllerService gifService;

    @Value("${file.cache-dir}")
    private String cacheDir;

    @GetMapping
    public List<GifsByQueryDto> getCache(@RequestParam(required = false) String query) {
        if (query == null) {
            return cacheService.findAll();
        } else {
            return List.of(cacheService.findByQuery(query));
        }
    }

    @PostMapping("/generate")
    public GifsByQueryDto generateGif(@RequestBody QueryDto queryDto) {
        gifService.findRandomGif(queryDto.getQuery(), cacheDir);

        return cacheService.findByQuery(queryDto.getQuery());
    }

    @DeleteMapping
    public void delete() {
        cacheService.deleteAll();
    }
}
