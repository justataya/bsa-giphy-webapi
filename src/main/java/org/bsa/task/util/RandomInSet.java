package org.bsa.task.util;

import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.Set;

@Component
public class RandomInSet {
    public String getRandomInSet(Set<String> set) {
        if (set.isEmpty()) {
            return null;
        }

        int item = new Random().nextInt(set.size());
        int i = 0;
        for (String gif : set) {
            if (i == item)
                return gif;
            i++;
        }

        return null;
    }
}
