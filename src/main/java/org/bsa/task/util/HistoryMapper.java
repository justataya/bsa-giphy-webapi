package org.bsa.task.util;

import lombok.RequiredArgsConstructor;
import org.bsa.task.dto.HistoryEntryDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class HistoryMapper {
    public HistoryEntryDto mapToDto(String[] historyEntry) {
        if (historyEntry.length < 3) {
            throw new IllegalArgumentException("History entry array must have length of 3");
        }
        return new HistoryEntryDto(historyEntry[0], historyEntry[1], historyEntry[2]);
    }

    public List<HistoryEntryDto> mapToListDto(List<String[]> gifsByQueryList) {
        return gifsByQueryList.stream().map(this::mapToDto).collect(Collectors.toList());
    }
}
