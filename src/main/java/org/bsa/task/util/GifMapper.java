package org.bsa.task.util;

import lombok.RequiredArgsConstructor;
import org.bsa.task.dto.GifsByQueryDto;
import org.bsa.task.entity.GifsByQuery;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;


@Component
@RequiredArgsConstructor
public class GifMapper {
    public GifsByQueryDto mapToDto(GifsByQuery gifsByQuery) {
        return new GifsByQueryDto(
                gifsByQuery.getQuery(),
                gifsByQuery.getGifs().stream().map(item -> ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/uploads/")
                        .path(item)
                        .toUriString()).collect(Collectors.toSet())
        );
    }

    public List<GifsByQueryDto> mapToListDto(List<GifsByQuery> gifsByQueryList) {
        return gifsByQueryList.stream().map(this::mapToDto).collect(Collectors.toList());
    }
}
