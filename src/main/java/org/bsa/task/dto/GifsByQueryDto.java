package org.bsa.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class GifsByQueryDto {
    private String query;
    private Set<String> gifs;
}
