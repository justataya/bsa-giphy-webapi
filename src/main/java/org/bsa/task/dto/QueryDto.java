package org.bsa.task.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QueryDto {
    private String query;
    private Boolean force;
}
