package org.bsa.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HistoryEntryDto {
    private String date;
    private String query;
    private String gif;
}
