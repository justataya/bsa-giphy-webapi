package org.bsa.task.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class GifsByQuery {
    String query;
    Set<String> gifs;
}
