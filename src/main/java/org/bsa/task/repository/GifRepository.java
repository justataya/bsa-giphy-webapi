package org.bsa.task.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Repository
public class GifRepository {
    private static final Logger logger = LoggerFactory.getLogger(GifRepository.class);

    @Value("${file.upload-dir}")
    private String dirPath;

    public String save(String relativePath, byte[] file) {
        try {
            Path path = checkPath(relativePath + ".gif");
            Files.write(path.toAbsolutePath(), file);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return relativePath + ".gif";
    }

    public String copy(String relativePath, String url) {
        Path sourcePath = Path.of(this.dirPath + url);
        try {
            Path path = checkPath(relativePath + "/" + sourcePath.getFileName());

            Files.copy(Path.of(this.dirPath + url), path.toAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return relativePath + "/" + sourcePath.getFileName();
    }

    private Path checkPath(String relativePath) throws IOException {
        Path path = Paths.get(this.dirPath + relativePath);
        Path parentDir = path.getParent();
        if (!Files.exists(parentDir)) {
            Files.createDirectories(parentDir);
        }
        return path;
    }

    public Set<String> findByPath(String relativePath) {
        Set<String> gifs = new HashSet<>();

        File dir = new File(this.dirPath + relativePath);
        if (!dir.exists() || dir.listFiles() == null) {
            return gifs;
        }
        for (final File fileEntry : Objects.requireNonNull(dir.listFiles())) {
            gifs.add(relativePath + "/" + fileEntry.getName());
        }
        return gifs;
    }

    public Set<String> findAllQueries(String relativePath) {
        Set<String> queries = new HashSet<>();

        File dir = new File(this.dirPath + relativePath);
        if (!dir.exists() || dir.listFiles() == null) {
            return queries;
        }
        for (final File fileEntry : Objects.requireNonNull(dir.listFiles())) {
            if (fileEntry.isDirectory()) {
                queries.add(fileEntry.getName());
            }
        }
        return queries;
    }
}
