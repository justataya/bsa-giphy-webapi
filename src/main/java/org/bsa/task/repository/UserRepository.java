package org.bsa.task.repository;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.bsa.task.util.RandomInSet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
@RequiredArgsConstructor
public class UserRepository {
    private static final Map<UUID, Map<String, Set<String>>> userCache = new HashMap<>();

    private static final String datePattern = "MM-dd-yyyy";

    private final RandomInSet randomInSet;

    @Value("${file.upload-dir}")
    private String dirPath;
    @Value("${file.users-dir}")
    private String usersDir;
    @Value("${file.history}")
    private String historyFile;


    public void saveHistory(Date date, String userId, String query, String gif) throws IOException {
        DateFormat dateFormat = new SimpleDateFormat(datePattern);

        CSVWriter writer = new CSVWriter(new FileWriter(checkPath(usersDir + "/" + userId + "/" + historyFile).toString(), true));
        writer.writeNext(new String[]{dateFormat.format(date), query, gif});
        writer.close();
    }

    public List<String[]> findHistoryByUserId(String userId) throws IOException {
        Reader reader = Files.newBufferedReader(checkPath(usersDir + "/" + userId + "/" + historyFile));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> list;
        list = csvReader.readAll();
        reader.close();
        csvReader.close();
        return list;
    }

    public void deleteHistory(String userId) throws IOException {
        Files.newBufferedWriter(checkPath(usersDir + "/" + userId + "/" + historyFile), StandardOpenOption.TRUNCATE_EXISTING);
    }

    public void saveCache(UUID userUuid, String query, String gif) {
        if (!userCache.containsKey(userUuid)) {
            userCache.put(userUuid, new HashMap<>());
        }

        if (!userCache.get(userUuid).containsKey(query)) {
            userCache.get(userUuid).put(query, new HashSet<>());
        }

        userCache.get(userUuid).get(query).add(ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/uploads/")
                .path(gif)
                .toUriString());
    }

    private Path checkPath(String relativePath) throws IOException {
        Path path = Paths.get(this.dirPath + relativePath);
        Path parentDir = path.getParent();
        if (!Files.exists(parentDir)) {
            Files.createDirectories(parentDir);
        }
        return path;
    }

    public void deleteUserCache(UUID userUuid) {
        userCache.remove(userUuid);
    }

    public void deleteUserCacheByQuery(UUID userUuid, String query) {
        userCache.get(userUuid).remove(query);
    }

    public void deleteAll(String userId) throws IOException {
        File dir = new File(this.dirPath + usersDir + "/" + userId);
        FileUtils.deleteDirectory(dir);
    }

    public String findGifInCache(UUID userUuid, String query) {
        if (!userCache.containsKey(userUuid) || !userCache.get(userUuid).containsKey(query) ) {
            return null;
        }

        Set<String> gifs = userCache.get(userUuid).get(query);

        return randomInSet.getRandomInSet(gifs);
    }
}
