package org.bsa.task.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class CacheRepository {
    @Value("${file.upload-dir}")
    private String dirPath;
    @Value("${file.cache-dir}")
    private String cacheDir;


    public void deleteAll() throws IOException {
        Path path = Paths.get(this.dirPath + cacheDir);
        Path parentDir = path.getParent();
        if (Files.exists(parentDir)) {
            FileUtils.cleanDirectory(new File(path.toString()));
        }
    }

}
