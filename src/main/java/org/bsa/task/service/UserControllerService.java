package org.bsa.task.service;

import lombok.RequiredArgsConstructor;
import org.bsa.task.dto.GifsByQueryDto;
import org.bsa.task.dto.HistoryEntryDto;
import org.bsa.task.exception.IllegalUserIdException;
import org.bsa.task.service.data.UserService;
import org.bsa.task.util.GifMapper;
import org.bsa.task.util.HistoryMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class UserControllerService {
    private final UserService userService;

    private final HistoryMapper historyMapper;
    private final GifMapper gifMapper;

    public List<GifsByQueryDto> findAll(String userId) {
        return gifMapper.mapToListDto(userService.findAll(userId));
    }

    public void validate(String userId) {
        if (!userId.matches("(?:[^\\\\/:*?\"<>|\\r\\n]+)")) {
            throw new IllegalUserIdException();
        }
    }

    public void saveHistory(String userId, String query, String gif) {
        userService.saveHistory(userId, query, gif);
        userService.saveCache(UUID.nameUUIDFromBytes(userId.getBytes()), query, gif);
    }

    public List<HistoryEntryDto> findHistoryByUserId(String userId) {
        return historyMapper.mapToListDto(userService.findHistoryByUserId(userId));
    }

    public void deleteHistory(String userId) {
        userService.deleteHistory(userId);
    }

    public void deleteUserCache(String userId) {
        userService.deleteUserCache(UUID.nameUUIDFromBytes(userId.getBytes()));
    }

    public void deleteUserCacheByQuery(String userId, String query) {
        userService.deleteUserCacheByQuery(UUID.nameUUIDFromBytes(userId.getBytes()), query);
    }

    public void deleteAll(String userId) {
        deleteUserCache(userId);
        userService.deleteAll(userId);
    }

    public String findGifInCache(String userId, String query) {
        return userService.findGifInCache(UUID.nameUUIDFromBytes(userId.getBytes()), query);
    }

    public String findGifOnDisk(String userId, String query) {
        String result = userService.findGifOnDisk(userId, query);
        if (result != null) {
            userService.saveCache(UUID.nameUUIDFromBytes(userId.getBytes()), query, result);
        }
        return result;
    }
}
