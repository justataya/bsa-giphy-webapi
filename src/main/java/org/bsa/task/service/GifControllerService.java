package org.bsa.task.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.bsa.task.service.data.GifService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GifControllerService {
    private static final Logger logger = LoggerFactory.getLogger(GifControllerService.class);

    private final GifService gifService;

    @Value("${giphy.api_key}")
    private String apiKey;
    @Value("${giphy.url.random}")
    private String requestUrl;

    @Value("${file.cache-dir}")
    private String cacheDir;

    public String findRandomGif(String query, String path) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(requestUrl)
                .queryParam("api_key", apiKey)
                .queryParam("tag", query);

        try {
            return gifService.findRandomGif(builder.toUriString(), path + "/" + query);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    public String copy(String query, String path, String url) {
        return gifService.copy(path + "/" + query, url);
    }

    public List<String> findAll() {
        return gifService.findAll(cacheDir);
    }
}
