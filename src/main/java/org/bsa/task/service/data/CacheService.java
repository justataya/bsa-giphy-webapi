package org.bsa.task.service.data;

import lombok.RequiredArgsConstructor;
import org.bsa.task.entity.GifsByQuery;
import org.bsa.task.repository.CacheRepository;
import org.bsa.task.repository.GifRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CacheService {
    private static final Logger logger = LoggerFactory.getLogger(CacheService.class);

    private final CacheRepository cacheRepository;
    private final GifRepository gifRepository;

    @Value("${file.cache-dir}")
    private String cacheDir;

    public List<GifsByQuery> findAll() {
        return gifRepository.findAllQueries(cacheDir).stream().map(this::findByQuery).collect(Collectors.toList());
    }

    public GifsByQuery findByQuery(String query) {
        return new GifsByQuery(query, gifRepository.findByPath(cacheDir + "/" + query));
    }

    public void deleteAll() {
        try {
            cacheRepository.deleteAll();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
