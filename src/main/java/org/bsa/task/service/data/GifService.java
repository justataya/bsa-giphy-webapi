package org.bsa.task.service.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.bsa.task.repository.GifRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GifService {
    private final RestTemplateBuilder restTemplate;

    private final GifRepository gifRepository;

    public String findRandomGif(String resourceUrl, String path) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.build().exchange(resourceUrl, HttpMethod.GET, entity, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode root = objectMapper.readTree(response.getBody());
        JsonNode id = root.path("data").path("id");
        JsonNode url = root.path("data").path("images").path("original").path("url");

        return findByUrl(path + "/" + id.asText(), url.asText());
    }


    public String copy(String path,String url ) {
        return gifRepository.copy(path, url);
    }

    private String findByUrl(String path, String url) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<byte[]> response = restTemplate.build()
                    .exchange(url, HttpMethod.GET, entity, byte[].class);
            return gifRepository.save(path, response.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Set<String> findByPath(String path) {
        return gifRepository.findByPath(path);
    }

    public List<String> findAll(String path) {
        return gifRepository.findAllQueries(path).stream().flatMap(item -> findByPath(path + "/" + item).stream()).collect(Collectors.toList());
    }
}
