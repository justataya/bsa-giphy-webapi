package org.bsa.task.service.data;

import lombok.RequiredArgsConstructor;
import org.bsa.task.entity.GifsByQuery;
import org.bsa.task.repository.GifRepository;
import org.bsa.task.repository.UserRepository;
import org.bsa.task.util.RandomInSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final GifRepository gifRepository;

    private final RandomInSet randomInSet;

    @Value("${file.users-dir}")
    private String usersDir;

    public List<GifsByQuery> findAll(String userId) {
        return gifRepository.findAllQueries(usersDir + "/" + userId).stream()
                .map(item -> new GifsByQuery(item, gifRepository.findByPath(usersDir + "/" + userId + "/" + item)))
                .collect(Collectors.toList());
    }

    public void saveHistory(String userId, String query, String gif) {
        Date date = new Date();

        try {
            userRepository.saveHistory(date, userId, query, gif);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public List<String[]> findHistoryByUserId(String userId) {
        try {
            return userRepository.findHistoryByUserId(userId);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return Collections.emptyList();
    }


    public void deleteHistory(String userId) {
        try {
            userRepository.deleteHistory(userId);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public void saveCache(UUID userUuid, String query, String gif) {
        userRepository.saveCache(userUuid, query, gif);
    }

    public void deleteUserCache(UUID userUuid) {
        userRepository.deleteUserCache(userUuid);
    }

    public void deleteUserCacheByQuery(UUID userUuid, String query) {
        userRepository.deleteUserCacheByQuery(userUuid, query);
    }

    public void deleteAll(String userId) {
        try {
            userRepository.deleteAll(userId);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String findGifInCache(UUID userUuid, String query) {
        return userRepository.findGifInCache(userUuid, query);
    }

    public String findGifOnDisk(String userId, String query) {
        Set<String> gifs = gifRepository.findByPath(usersDir + "/" + userId + "/" + query);

        return randomInSet.getRandomInSet(gifs);
    }
}
