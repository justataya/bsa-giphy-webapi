package org.bsa.task.service;

import lombok.RequiredArgsConstructor;
import org.bsa.task.dto.GifsByQueryDto;
import org.bsa.task.service.data.CacheService;
import org.bsa.task.util.GifMapper;
import org.bsa.task.util.RandomInSet;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CacheControllerService {
    private final CacheService cacheService;
    private final GifMapper gifMapper;

    private final RandomInSet randomInSet;

    public List<GifsByQueryDto> findAll() {
        return gifMapper.mapToListDto(cacheService.findAll());
    }

    public String findRandomByQuery(String query) {
        Set<String> gifs = cacheService.findByQuery(query).getGifs();

        return randomInSet.getRandomInSet(gifs);
    }

    public GifsByQueryDto findByQuery(String query) {
        return gifMapper.mapToDto(cacheService.findByQuery(query));
    }

    public void deleteAll() {
        cacheService.deleteAll();
    }
}
